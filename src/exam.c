#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>
#include <unistd.h>

int main(int argc, char *argv[])
{
    int flags, opt;
    FILE *inputFile;
    FILE *outputFile;
    int i;
    int compteur =0;

    if ((opt = getopt(argc, argv, "pic:")) != -1)
    {
        switch (opt)
        {
        // Afficher le contenu d'un fichier
        case 'p':
            inputFile = fopen(argv[2], "r");
            if (inputFile == NULL)
            {
                printf("fichier vide \n");
                return 1;
            }
            while ((i = fgetc(inputFile)) != EOF)
            {
                fputc(i, stdout);
            }
            fclose(inputFile);
            break;
        // écrire les lignes lues dans un nouveau fichier
        case 'i':
            inputFile = fopen(argv[2], "r");
            outputFile = fopen("../samples/output.txt", "w");
            if (inputFile == NULL)
            {
                printf("fichier vide \n");
                return 1;
            }
            while ((i = fgetc(inputFile)) != EOF)
            {
                fputc(i, outputFile);
            }
            fclose(inputFile);
            fclose(outputFile);
            break;
        // Calculer le nombre de mots
        case 'c':
            inputFile = fopen(argv[2], "r");
            if (inputFile == NULL)
            {
                printf("fichier vide, pas de mots \n");
            }
            while (i = fgetc(inputFile) != EOF)
            {
            
                compteur = compteur + 1;

            }
            return compteur;
        // à finir
        case 'r':
            break;
        // à finir
        case 's':
            break;
        }
    }

    return 0;
}
